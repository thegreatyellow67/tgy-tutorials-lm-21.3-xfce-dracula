#!/bin/bash

clear

# ###############################################################
#
# VARIABILI USATE DALLO SCRIPT, NON MODIFICARE!
#
THEME="dracula"
CURSOR_THEME="dracula"
ICON_THEME="tela-circle-dracula"
THEME_FOLDER="lm-21.3-xfce-${THEME}"
BASE_PATH="${HOME}/Scaricati"
COLOR_SCHEMES_FILE="${BASE_PATH}/color-schemes"
PANEL_PROFILE="Dracula-Slim"
#
# ###############################################################

if [ ! -f "${COLOR_SCHEMES_FILE}" ]; then
  wget https://gitlab.com/thegreatyellow67/tgy-tutorials-lm-21.3-xfce-${THEME}/-/raw/main/color-schemes &> /dev/null
fi
source $(dirname $0)/color-schemes

echo -e "${blue}"
echo ""
echo "  ██╗  ██╗███████╗ ██████╗███████╗"
echo "  ╚██╗██╔╝██╔════╝██╔════╝██╔════╝"
echo "   ╚███╔╝ █████╗  ██║     █████╗"
echo "   ██╔██╗ ██╔══╝  ██║     ██╔══╝"
echo "  ██╔╝ ██╗██║     ╚██████╗███████╗"
echo "  ╚═╝  ╚═╝╚═╝      ╚═════╝╚══════╝"
echo ""
echo "  ██████╗ ██████╗  █████╗  ██████╗██╗   ██╗██╗      █████╗"
echo "  ██╔══██╗██╔══██╗██╔══██╗██╔════╝██║   ██║██║     ██╔══██╗"
echo "  ██║  ██║██████╔╝███████║██║     ██║   ██║██║     ███████║"
echo "  ██║  ██║██╔══██╗██╔══██║██║     ██║   ██║██║     ██╔══██║"
echo "  ██████╔╝██║  ██║██║  ██║╚██████╗╚██████╔╝███████╗██║  ██║"
echo "  ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝"
echo -e "${red}${bold}"
echo "  =========================================================="
echo "  Script per automatizzare la copia delle"
echo "  risorse per il tema Xfce ${THEME^^}"
echo "  Scritto da TGY-TUTORIALS il 13/04/2024"
echo "  Versione: 1.3"
echo "  Ultima modifica: 18/04/2024"
echo "  =========================================================="
echo -e "${reset}"

function goto
{
  label=$1
  cmd=$(sed -n "/$label:/{:a;n;p;ba};" $0 | grep -v ':$')
  eval "$cmd"
  exit
}

echo "  Premi 's' per continuare o 'n' per uscire dallo script..."

# In attesa che l'utente prema un tasto
read -s -n 1 key

# Controlla se è stato premuto un tasto
case $key in
  s|S)
    goto main
    ;;
  n|N)
    echo "  Termino lo script...a presto!"
    exit 1
    ;;
  *)
    echo "  Tasto non valido. Per favore premi 's' o 'n'."
    sleep 5
    exit 1
    ;;
esac

main:

#
# Crea la cartella Scaricati se
# eventualmente mancante
#
if [ ! -d ${BASE_PATH} ];then
  mkdir -p ${BASE_PATH}
fi

cd ${BASE_PATH}

if [ ! -d ${THEME_FOLDER} ];then

  echo ""
  echo "  Sto per scaricare le risorse da GitLab, un pò di pazienza..."
  echo ""

  #
  # Installa git se mancante
  #
  if ! location="$(type -p "git")" || [ -z "git" ]; then
    echo "  Installo git per far funzionare questo script..."
    echo ""
    sudo apt install -y git &> /dev/null
  fi

  git clone https://gitlab.com/thegreatyellow67/tgy-tutorials-lm-21.3-xfce-${THEME}.git ${THEME_FOLDER}

  cd ${THEME_FOLDER}
  rm -fr .git

  clear
  echo ""
  echo "  Sto installando alcuni pacchetti utili, un pò di pazienza..."
  echo "  ======================================================================================="
  echo "   Pacchetti:"
  echo "   brightnessctl | btop | cava | conky-all | dconf-editor | ffmpeg | fonts-powerline"
  echo "   gimp | git | gist | goodvibes | gpick | gnome-calendar | htop | inkscape | jq"
  echo "   libxfce4ui-utils (xfce4-about) | nala | pinentry-gtk2 | playerctl | rofi | ruby"
  echo "   ruby-dbus | vlc | xclip | xfce4-clipman-plugin | xfce4-panel-profiles | yad | zenity"
  echo "  ======================================================================================="
  echo ""
  sudo apt install brightnessctl btop cava conky-all dconf-editor ffmpeg fonts-powerline gimp git gist goodvibes gpick gnome-calendar htop inkscape jq libxfce4ui-utils nala pinentry-gtk2 playerctl rofi ruby ruby-dbus vlc xclip xfce4-clipman-plugin xfce4-panel-profiles yad zenity -y &> /dev/null

  #
  # Crea cartelle eventualmente mancanti
  #
  if [ ! -d ~/.fonts ];then
    mkdir -p ~/.fonts
  fi

  if [ ! -d ~/.themes ];then
    mkdir -p ~/.themes
  fi

  if [ ! -d ~/.icons ];then
    mkdir -p ~/.icons
  fi

  if [ ! -d ~/.config/conky ];then
    mkdir -p ~/.config/conky
  fi

  if [ ! -d ~/.config/autostart ];then
    mkdir -p ~/.config/autostart
  fi

  if [ ! -d ~/.local/share/applications ];then
    mkdir -p ~/.local/share/applications
  fi

  if [ ! -d /boot/grub/themes ];then
    sudo mkdir -p /boot/grub/themes
  fi

  clear
  echo ""
  echo "  Sostituzione con utente corrente in"
  echo "  alcuni files di configurazione..."
  echo ""
  find configs/.config/ -type f -exec sed -i "s/tgy-tutorials/${USER}/g" {} +
  find configs/.local/ -type f -exec sed -i "s/tgy-tutorials/${USER}/g" {} +
  find configs/xfce-config-helper/ -type f -exec sed -i "s/tgy-tutorials/${USER}/g" {} +
  find user-scripts/.scripts/ -type f -exec sed -i "s/tgy-tutorials/${USER}/g" {} +

  # Sostituzione con utente corrente nella configurazione
  # per i pannelli da importare con xfce4-panel-profiles
  mkdir -p configs/.local/share/xfce4-panel-profiles/${PANEL_PROFILE}/
  tar -xf configs/.local/share/xfce4-panel-profiles/${PANEL_PROFILE}.tar.bz2 -C configs/.local/share/xfce4-panel-profiles/${PANEL_PROFILE}/
  rm configs/.local/share/xfce4-panel-profiles/${PANEL_PROFILE}.tar.bz2
  find configs/.local/share/xfce4-panel-profiles/${PANEL_PROFILE}/ -type f -exec sed -i "s/tgy-tutorials/${USER}/g" {} +
  cd configs/.local/share/xfce4-panel-profiles/${PANEL_PROFILE}/
  tar -cvjSf ${BASE_PATH}/${THEME_FOLDER}/configs/.local/share/xfce4-panel-profiles/${PANEL_PROFILE}.tar.bz2 * &>/dev/null
  cd ${BASE_PATH}/${THEME_FOLDER}
  rm -fr configs/.local/share/xfce4-panel-profiles/${PANEL_PROFILE}/
  sleep 3

  clear
  echo ""
  echo "  Importazione della configurazione dei pannelli"
  echo "  tramite xfce4-panel-profiles..."
  echo ""
  /usr/bin/xfce4-panel-profiles load configs/.local/share/xfce4-panel-profiles/${PANEL_PROFILE}.tar.bz2
  sleep 3

  clear
  echo ""
  echo "  Installazione dei caratteri..."
  echo ""
  echo "  Caratteri utilizzati in questo tema:"
  echo ""
  echo "  Agave Nerd Font:"
  echo "  ==================================================================================="
  echo "  https://github.com/ryanoasis/nerd-fonts/releases/download/v3.2.1/Agave.zip"
  echo "  ==================================================================================="
  echo ""
  echo "  JetBrainsMono Nerd Font (alternativo, per documenti):"
  echo "  ==================================================================================="
  echo "  https://github.com/ryanoasis/nerd-fonts/releases/download/v3.1.1/JetBrainsMono.zip"
  echo "  ==================================================================================="
  echo ""
  cp -r ${THEME}-fonts/* ~/.fonts
  sudo cp -r ${THEME}-fonts/Agave/ /usr/share/fonts/truetype/
  sudo cp -r ${THEME}-fonts/JetBrainsMono/ /usr/share/fonts/truetype/
  fc-cache -fr
  sudo fc-cache -fr
  sleep 3

  clear
  echo ""
  echo "  Installazione del tema GTK ${THEME^^}, un pò di pazienza..."
  echo ""
  echo "  Fonti:"
  echo "  ====================================="
  echo "  https://github.com/dracula/gtk"
  echo "  https://www.gnome-look.org/p/1687249"
  echo "  ====================================="
  echo ""
  cp -r ${THEME}-gtk-themes/* ~/.themes
  sudo cp -r ${THEME}-gtk-themes/* /usr/share/themes
  sleep 3

  clear
  echo ""
  echo "  Installazione delle icone ${ICON_THEME^^}, un pò di pazienza..."
  echo ""
  echo "  Fonti:"
  echo "  ======================================================"
  echo "  https://github.com/vinceliuice/Tela-circle-icon-theme"
  echo "  https://www.gnome-look.org/p/1359276"
  echo "  ======================================================"
  echo ""
  cp -r ${THEME}-icons/* ~/.icons
  sudo cp -r ${THEME}-icons/* /usr/share/icons
  sleep 3

  clear
  echo ""
  echo "  Installazione dei cursori ${CURSOR_THEME^^}, un pò di pazienza..."
  echo ""
  echo "  Fonti:"
  echo "  ======================================================="
  echo "  https://github.com/dracula/gtk/tree/master/kde/cursors"
  echo "  https://www.gnome-look.org/p/1669262/"
  echo "  ======================================================="
  echo ""
  sudo cp -r ${THEME}-cursors/* /usr/share/icons
  sleep 3

  clear
  echo ""
  echo "  Installazione degli sfondi..."
  echo ""
  cp -r ${THEME}-backgrounds/* ~/Immagini
  sleep 3

  clear
  echo ""
  echo "  Installazione di icone varie usate dal tema..."
  echo ""
  echo "  =========================================================================="
  echo "  - Icone personalizzate per i plugins del pannello (create da me)"
  echo "  - Icone per le azioni personalizzate di Thunar (create da me)"
  echo "  - Icone per il plugin del Meteo:"
  echo "    icone clima: https://www.gnome-look.org/p/1165881"
  echo "    icone mono: https://github.com/kevin-hanselman/xfce4-weather-mono-icons"
  echo "  =========================================================================="
  echo ""
  read -n 1 -r -s -p $'  Premi INVIO per continuare...\n'
  cp -r misc-icons/.icons/* ~/.icons
  sudo cp -r misc-icons/usr/share/xfce4/weather/icons/* /usr/share/xfce4/weather/icons/ 

  clear
  echo ""
  echo "  Installazione di configurazioni varie e utilità di ripristino..."
  echo ""
  echo "  =========================================================================="
  echo "  - File per avvio automatico di applicazioni (autostart)"
  echo "  - Script di Conky"
  echo "    tema originale: https://www.gnome-look.org/p/1957785"
  echo "  - Avviatore di applicazioni Plank"
  echo "  - Gestore files Thunar"
  echo "  - Azioni personalizzate di Thunar"
  echo "    https://github.com/cytopia/thunar-custom-actions"
  echo "  - Rofi Menu e PowerMenu"
  echo "  - Visualizzatore audio Cava"
  echo "  - Visualizzatore audio Glava"
  echo "  - macchina"
  echo "  - neofetch"
  echo "  - Impostazioni di Xfce"
  echo "  - Impostazioni del terminale di Xfce"
  echo "    tema: https://draculatheme.com/xfce4-terminal"
  echo "  - Utilità per ripristino impostazioni di Xfce (xfconf-load e xfconf-dump)"
  echo "  - File weather.json per le impostazioni del meteo di Conky"
  echo "  =========================================================================="
  echo ""
  read -n 1 -r -s -p $'  Premi INVIO per continuare...\n'
  cp -r configs/.config/* ~/.config
  cp -r configs/.local/bin/ ~/.local/
  cp -r configs/.local/share/* ~/.local/share/
  cp configs/.cache/weather.json ~/.cache/
  dconf load /net/launchpad/plank/ < configs/plank-settings.conf

  clear
  echo ""
  echo "  Sostituzione del dipositivo di rete WiFi o LAN"
  echo "  nello script di Conky..."
  echo ""

  WIFI_DEVICE=`nmcli device | grep wifi\ | awk '{print $1}'`
  LAN_DEVICE=`ip -br l | awk '$1 !~ "lo|vir|wl" { print $1}'`

  # Sostituzione dei dipositivi di rete WiFi o LAN
  if [[ "${WIFI_DEVICE}" != "" ]]; then
    sed -i "s/wlan0/${WIFI_DEVICE}/g" ~/.config/conky/Fomalhaut_Violet/Fomalhaut1.conf
  fi

  if [[ "${LAN_DEVICE}" != "" ]]; then
    sed -i "s/enp0s3/${LAN_DEVICE}/g" ~/.config/conky/Fomalhaut_Violet/Fomalhaut1.conf
  fi
  sleep 3

  clear
  echo ""
  echo "  Installazione di scripts utente personalizzati..."
  echo ""
  cp -r user-scripts/.scripts ~/
  sleep 3

  clear
  echo ""
  echo "  Copia di un avatar generico per l'utente..."
  echo ""
  echo "  NOTA BENE:"
  echo "  ======================================================================"
  echo "  nella cartella ${THEME_FOLDER}/resources/avatars/ puoi"
  echo "  trovare differenti icone avatar per rappresentare il tuo utente."
  echo "  Basta copiare quella di tuo gradimento nella cartella ${HOME} e"
  echo "  rinominarla in .face"
  echo "  ======================================================================"
  echo ""
  read -n 1 -r -s -p $'  Premi INVIO per continuare...\n'
  cp -r resources/avatars/avatar-male-female.svg ~/.face

  clear
  echo ""
  echo "  Installazione di macchina..."
  echo ""
  sudo cp macchina/macchina-linux-x86_64 /usr/local/bin/macchina
  sudo chmod +x /usr/local/bin/macchina
  sleep 3

  clear
  echo ""
  echo "  Installazione di RadioTray-NG..."
  echo ""
  sudo dpkg -i radiotray-ng/radiotray-ng_0.2.8_ubuntu_22.04_amd64.deb &> /dev/null
  sudo apt install -fy &> /dev/null
  sleep 3

  clear
  echo ""
  echo "  Installazione dello sfondo per la finestra di accesso..."
  echo ""
  sudo cp -r login-window/ /usr/share/backgrounds
  sleep 3

  clear
  echo ""
  echo "  Installazione del tema Sugar Candy Dracula per Grub..."
  echo ""
  sudo cp -r sugar-candy-dracula/ /boot/grub/themes
  sleep 3

  clear
  echo ""
  echo "  Installazione del tema spinner_alt per Plymouth..."
  echo ""
  echo "  Nel terminale si dovranno poi eseguire i seguenti comandi:"
  echo "  ========================================================================"
  echo "  $ sudo update-alternatives --config default.plymouth (scegliere tema 3)"
  echo "  $ sudo update-initramfs -u"
  echo "  ========================================================================"
  echo ""
  read -n 1 -r -s -p $'  Premi INVIO per continuare...\n'
  sudo cp -r plymouth-theme/spinner_alt/ /usr/share/plymouth/themes/
  sudo update-alternatives --install /usr/share/plymouth/themes/default.plymouth default.plymouth /usr/share/plymouth/themes/spinner_alt/spinner_alt.plymouth 100

  clear
  echo ""
  echo "  Importazione delle impostazioni di Xfce4 tramite xfconf-load..."
  echo "  Fonte: https://github.com/felipec/xfce-config-helper"
  echo "  ================================================================"
  echo "  Per esportare le impostazioni di Xfce4:"
  echo "  xfconf-dump > config.yml"
  echo ""
  echo "  Per importare le impostazioni di Xfce4:"
  echo "  xfconf-load config.yml"
  echo "  ================================================================"
  echo ""
  read -n 1 -r -s -p $'  Premi INVIO per continuare...\n'
  ${HOME}/.local/bin/xfconf-load configs/xfce-config-helper/xfce4-settings.yml &>/dev/null

  clear
  echo ""
  echo "  Risorse installate con successo!"
  echo ""
  sleep 3

  clear
  echo ""
  echo "  ██████╗ ██╗ █████╗ ██╗   ██╗██╗   ██╗██╗ ██████╗"
  echo "  ██╔══██╗██║██╔══██╗██║   ██║██║   ██║██║██╔═══██╗"
  echo "  ██████╔╝██║███████║██║   ██║██║   ██║██║██║   ██║"
  echo "  ██╔══██╗██║██╔══██║╚██╗ ██╔╝╚██╗ ██╔╝██║██║   ██║"
  echo "  ██║  ██║██║██║  ██║ ╚████╔╝  ╚████╔╝ ██║╚██████╔╝"
  echo "  ╚═╝  ╚═╝╚═╝╚═╝  ╚═╝  ╚═══╝    ╚═══╝  ╚═╝ ╚═════╝"
  echo ""
  echo "  ██████╗ ███████╗██╗         ██████╗  ██████╗"
  echo "  ██╔══██╗██╔════╝██║         ██╔══██╗██╔════╝"
  echo "  ██║  ██║█████╗  ██║         ██████╔╝██║"
  echo "  ██║  ██║██╔══╝  ██║         ██╔═══╝ ██║"
  echo "  ██████╔╝███████╗███████╗    ██║     ╚██████╗    ██╗██╗██╗"
  echo "  ╚═════╝ ╚══════╝╚══════╝    ╚═╝      ╚═════╝    ╚═╝╚═╝╚═╝"
  echo ""
  read -n 1 -r -s -p $'  Premi INVIO per continuare...\n'
  sudo systemctl reboot

else
  echo "  La cartella ${THEME_FOLDER} esiste! se vuoi scaricare"
  echo "  le risorse nuovamente devi cancellare la cartella e"
  echo "  rieseguire questo script in un terminale. Buona giornata!"
  echo ""
fi
