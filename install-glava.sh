#!/bin/bash

clear

# ###############################################################
#
# VARIABILI USATE DALLO SCRIPT, NON MODIFICARE!
#
THEME="dracula"
THEME_FOLDER="lm-21.3-xfce-${THEME}"
BASE_PATH="${HOME}/Scaricati"
COLOR_SCHEMES_FILE="${BASE_PATH}/color-schemes"
#
# ###############################################################

if [ ! -f "${COLOR_SCHEMES_FILE}" ]; then
  wget https://gitlab.com/thegreatyellow67/tgy-tutorials-lm-21.3-xfce-dracula/-/raw/main/color-schemes &> /dev/null
fi
source $(dirname $0)/color-schemes

echo -e "${blue}"
echo ""
echo "  ██╗  ██╗███████╗ ██████╗███████╗"
echo "  ╚██╗██╔╝██╔════╝██╔════╝██╔════╝"
echo "   ╚███╔╝ █████╗  ██║     █████╗"
echo "   ██╔██╗ ██╔══╝  ██║     ██╔══╝"
echo "  ██╔╝ ██╗██║     ╚██████╗███████╗"
echo "  ╚═╝  ╚═╝╚═╝      ╚═════╝╚══════╝"
echo ""
echo "  ██████╗ ██████╗  █████╗  ██████╗██╗   ██╗██╗      █████╗"
echo "  ██╔══██╗██╔══██╗██╔══██╗██╔════╝██║   ██║██║     ██╔══██╗"
echo "  ██║  ██║██████╔╝███████║██║     ██║   ██║██║     ███████║"
echo "  ██║  ██║██╔══██╗██╔══██║██║     ██║   ██║██║     ██╔══██║"
echo "  ██████╔╝██║  ██║██║  ██║╚██████╗╚██████╔╝███████╗██║  ██║"
echo "  ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝"
echo -e "${red}${bold}"
echo "  ===================================================================="
echo "  Script per installare il visualizzatore grafico Glava"
echo "  Scritto da TGY-TUTORIALS il 13/04/2024"
echo "  Versione: 1.0"
echo "  Ultima modifica: 18/04/2024"
echo "  ===================================================================="
echo -e "${reset}"

function goto
{
  label=$1
  cmd=$(sed -n "/$label:/{:a;n;p;ba};" $0 | grep -v ':$')
  eval "$cmd"
  exit
}

echo "  Premi 's' per continuare o 'n' per uscire dallo script..."

# In attesa che l'utente prema un tasto
read -s -n 1 key

# Controlla se è stato premuto un tasto
case $key in
  s|S)
    goto main
    ;;
  n|N)
    echo "  Termino lo script...a presto!"
    exit 1
    ;;
  *)
    echo "  Tasto non valido. Per favore premi 's' o 'n'."
    sleep 5
    exit 1
    ;;
esac

main:

echo ""
echo "  Sto installando alcuni pacchetti per la compilazione di glava, un pò di pazienza..."
echo ""

sudo apt install libgl1-mesa-dev libpulse0 libpulse-dev libxext6 libxext-dev libxrender-dev libxcomposite-dev liblua5.3-dev liblua5.3-0 lua-lgi lua-filesystem libobs0 libobs-dev meson build-essential gcc ccache -y &> /dev/null

sudo ldconfig
sudo ccache -c

#
# Installa git se mancante
#
if ! location="$(type -p "git")" || [ -z "git" ]; then
  echo "  Installo git per far funzionare questo script..."
  sudo apt install -y git &> /dev/null
fi

cd ${BASE_PATH}
git clone https://gitlab.com/wild-turtles-publicly-release/glava/glava.git glava-src-gitlab
cd ${BASE_PATH}/glava-src-gitlab

echo ""
echo "  Compilo glava, un pò di pazienza..."
echo ""

#meson build --reconfigure --prefix /usr
meson build --prefix /usr
ninja -C build
sudo ninja -C build install

