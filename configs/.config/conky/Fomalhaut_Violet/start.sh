#!/bin/bash

# This command will close all active conky
killall conky
sleep 2s
		
# Only the config listed below will be avtivated
# if you want to combine with another theme, write the command here	
conky -c $HOME/.config/conky/Fomalhaut_Violet/Fomalhaut1.conf &> /dev/null &
conky -c $HOME/.config/conky/Fomalhaut_Violet/Fomalhaut2.conf &> /dev/null &

exit
